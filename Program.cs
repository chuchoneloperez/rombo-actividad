﻿using System;

class Program
{
    static void Main()
    {
        int n =5 ; // Puedes ajustar el tamaño cambiando este valor

        if (n > 0)
        {
            char[,] diamondMatrix = new char[n * 2 - 1, n * 2 - 1];

            // Inicializar la matriz con espacios en blanco
            for (int i = 0; i < n * 2 - 1; i++)
            {
                for (int j = 0; j < n * 2 - 1; j++)
                {
                    diamondMatrix[i, j] = ' ';
                }
            }

            // Llenar la matriz con asteriscos para la parte superior del rombo
            for (int i = 0; i < n; i++)
            {
                for (int j = n - 1 - i; j <= n - 1 + i; j++)
                {
                    diamondMatrix[i, j] = '*';
                }
            }

            // Llenar la matriz con asteriscos para la parte inferior del rombo
            for (int i = n; i < n * 2 - 1; i++)
            {
                for (int j = i - n + 1; j < n * 2 - 1 - (i - n); j++)
                {
                    diamondMatrix[i, j] = '*';
                }
            }

            // Imprimir la matriz en la consola
            for (int i = 0; i < n * 2 - 1; i++)
            {
                for (int j = 0; j < n * 2 - 1; j++)
                {
                    Console.Write(diamondMatrix[i, j]);
                }
                Console.WriteLine();
            }

            Console.ReadLine(); // Esperar la entrada del usuario antes de cerrarse
        }
       
    }
}
